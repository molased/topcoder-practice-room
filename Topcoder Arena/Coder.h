#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <math.h>
#include <complex>
#include <iostream>
#include <sstream>
#include <limits.h>
#include <string.h>
#include<queue>
#include <stack>
#include <set>
#include <map>
using namespace std;

typedef double dd;
typedef long long LL;
typedef vector<double> VD;
typedef vector<bool> VB;
typedef vector<LL> VL;
typedef vector<VD> VVD;
typedef vector<string> VS;
typedef vector<int> VI;
typedef vector<VL> VLL;
typedef vector<VI> VVI;
typedef vector<VS> VVS;


#define PI 3.14159265359
#define inf 2000000000
#define ALL(n) (n).begin(),(n).end()
#define eps 1e-7
//#define SPRINTF(a) (*({static string _sprintf_s; char _sprintf_b[4096]; sprintf( _sprintf_b, a); _sprintf_s = _sprintf_b; &_sprintf_s;}))
#define PB push_back
#define S(n) (int)n.size()
#define REP(i,s,n) for(int i=s;i<(int)(n);i++)
#define RE(i,n) for(int i=0;i<(int)(n);i++)
#define REV(i,n) for(int i=n-1;i>-1;i--)
#define REPV(i,e,n) for(int i=n-1;i>=e;i--)
#define CLEAR(x,with) memset(x,with,sizeof(x))
#define SORT(n) sort(ALL(n))
#define SORTR(n) sort(n.rbegin(),n.rend())
#define ACCU(n,s) accumulate(ALL(n),s);
#define RV(n) reverse(ALL(n))
#define VAR(a,b) decltype(b) a=b
#define FORIT(it,v) for(VAR(it,(v).begin());it!=(v).end();++(it))
#define FOREACH(ite,n) for(ite = (n).begin();ite != (n).end();ite++)

#define two(X) (1<<(X))
#define contain(S,X) (((S)&two(X))!=0)
template<class T> inline int countbit(T n){ return (n == 0) ? 0 : (1 + countbit(n&(n - 1))); }

LL GCD(LL a, LL b) { while (a%b != 0) { LL c = a; a = b, b = c%b; } return b; }
template<class T, class U> T cast(U x) { T y; ostringstream a; a << x; istringstream b(a.str()); b >> y; return y; }
template<class T> vector<T> split(string s, string x = " ") { vector<T> r; RE(i, s.size()) { string c; while (i<(int)s.size() && x.find(s[i]) == string::npos) c += s[i++]; if (c.size()) r.push_back(cast<T>(c)); } return r; }
string strlower(string s) { for (string::iterator si = s.begin(); si != s.end(); ++si) *si = tolower(*si); return s; }

template<class T> vector<vector<T> > splitv(VS vs, string x = " ") { vector<vector<T> > r; RE(i, S(vs)) r.PB(split<T>(vs[i], x)); return r; }

//int dx[] = { 1, 0, 0, -1, -1, -1 };
//int dy[] = { 0, 1, -1, 0, 1, -1 };
int dxo[] = { 1, 1, 2, 2, -1, -1, -2, -2 };
int dyo[] = { 2, -2, 1, -1, 2, -2, 1, -1 };

int dx[] = { 0, -1, 0, 1 };
int dy[] = { 1, 0,-1,0 };

int dz[] = { 0, 0, 0, 0, 1, -1 };

class TreeUnionDiv2
{
public:
    set<VI> taken;
    int a[2][2][10][10];
    int b[2][2][10][10];
    
//    int maximumCycles(vector <string> tree1, vector <string> tree2, int K)
//    {
//        
//        int n= S(tree1);
//        CLEAR(a,0);
//        RE(i,n)RE(j,n) a[0][0][i][j] = tree1[i][j] == 'x';
//        RE(i,n)RE(j,n) a[1][1][i][j] = tree2[i][j] == 'x' ;
//        VI v; RE(i,n)v.push_back(i);
//        int res = 0;
//        do {
//            CLEAR(a[0][1],0),CLEAR(a[1][0],0);CLEAR(b,0);
//            RE(i,n)a[0][1][i][v[i]]= 1, a[1][0][v[i]][i] = 1;
//            taken.clear();
//            RE(i,2)RE(j,n) f1(j,i,j,i);
//            res = max(res,S(taken));
//            
//        } while (next_permutation(ALL(v)));
//        return res;
//    }
//    int maximumCycles2(vector <string> tree1, vector <string> tree2, int K)
//    {
//        
//        int n= S(tree1);
//        CLEAR(a,0),CLEAR(a1,0),CLEAR(a2,0),CLEAR(a3,0);
//        
//        RE(i,S(tree2)) tree2[i] = string(S(tree1),'-'),tree1.push_back(tree2[i]);
//        int r = 0;
//        VI v; RE(i,n)v.push_back(i);
//        do {
//            VS t = tree1;
//            RE(i,n)t[i][n+v[i]] = 'x',t[n+v[i]][i] = 'x';
//            //r = max(r,f1(t,K));
//        } while (next_permutation(ALL(v)));
//        return r;
//    }
};
class WallGameDiv2
{
public:
    VS costs;
    int dp[51][51],dp2[51][51],dp3[51][51][3];
    int n,m;
    int f1(int x,int y,int co)
    {
        if(dp[x][y]) return 0;
        if(x >= n || x < 0 || y >= m || y < -1 || costs[x][y] == 'x') return 0;
        int rr = costs[x][y]-'0';
        if(x == n-1) return rr+co;
        if(dp2[x][y] != -1 && dp2[x][y] >= co+rr)  return dp2[x][y];
        dp[x][y] = 1;

        
        int r = -inf;
        r = max(r,f1(x,y+1,co+rr));
        r = max(r,f1(x,y-1,co+rr));
        r = max(r,f1(x+1,y,co+rr));
        
        dp[x][y] = 0;
        return (dp2[x][y] = r);
    }
    int f2(int x,int y)
    {
        if(dp[x][y]) return 0;
        if(dp2[x][y] != -1)  return dp2[x][y];
        dp[x][y] = 1;
        if(x == n-1) return costs[x][y]-'0';
        if(x >= n || x < 0 || y >= m || y < -1 || dp[x][y] == 'x') return 0;
        int r = -inf;
        int rr = 0;
        if((rr = f2(x,y+1)))r=max(r,costs[x][y]-'0'+rr);
        if((rr = f2(x,y+1)))r=max(r,costs[x][y]-'0'+rr);
        if((rr = f2(x,y+1)))r=max(r,costs[x][y]-'0'+rr);
        dp[x][y] = 0;
        return (dp2[x][y] = r);
        
    }
//    void f1(int x,int y,int c)
//    {
//        if(dp[x][y]) return;
//        if(x == S(costs)) { r = min(r,c); return;}
//        dp[x][y] = 1;
//        if(y+1<S(costs[x]) && costs[x][y+1] != 'x') f1(x,y+1,c+costs[x][y]-'0');
//        if(y-1>-1 && costs[x][y-1] != 'x') f1(x,y-1,c+costs[x][y]-'0');
//        if(x+1<S(costs) && costs[x+1][y] != 'x') f1(x+1,y,c+costs[x][y]-'0');
//        dp[x][y] = 0;
//    }
    int f3(int x,int y,int dr)
    {
        if(dp[x][y]) return -100000;
        if(x < 0 || y < 0 || y >= m || costs[x][y] == 'x') return -100000;
        if(x == n-1) return costs[x][y]-'0';
        if(dp3[x][y][dr] != -1) return dp3[x][y][dr];
        dp[x][y] = 1;
        int maxv = 0;
        maxv = max(maxv,f3(x,y+1,2) + costs[x][y]-'0');
        maxv = max(maxv, f3(x,y-1,1) + costs[x][y]-'0');
        maxv = max(maxv,f3(x+1,y,0)+costs[x][y]-'0');
        dp[x][y] = 0;
        
        return (dp3[x][y][dr] =  maxv);
        
    }
    int play(vector <string> costss)
    {
        CLEAR(dp,0);
        CLEAR(dp2,-1);
        costs= costss;
        n = S(costs),m = S(costs[0]);
        return f1(0,0,0);
    }
};
class EllysCoprimesDiv2
{
public:
    int d;
    int getCount(vector <int> numbers)
    {
        SORT(numbers);
        int c = 0;
        RE(i,S(numbers)-1)
        {
            if(GCD(numbers[i],numbers[i+1]) != 1)
            {
                c+=2;
                REP(j,numbers[i]+1,numbers[i+1]) if(GCD(numbers[i], j) == 1 && GCD(numbers[i+1], j) == 1) {c--; break;}
            }
                   
        }
        return c;
    }
    void between(int a,int b,int c)
    {
        if(c > d) return;
        if(GCD(a,b) == 1) {d = c; return;}
        
        REP(i,a+1,b) between(i,b,c+1);
    }
   
    int getCount1(vector <int> numbers)
    {
        int c = 0;
        SORT(numbers);
        RE(i,S(numbers)-1)
        {
            d = inf;
            between(numbers[i], numbers[i+1], 0);
            c+=d;
        }
        return c;
    }
};
class CharacterBoard2
{
public:
    int countGenerators(vector <string> fragment, int W, int i0, int j0)
    {
        int r =0 ;
        int c[10000];
        LL a = i0*W+j0;
        REP(k,1,W+1)
        {
            LL b = a%k;
            CLEAR(c,-1);
            bool ok = 1;
            RE(i,S(fragment))RE(j,S(fragment[i]))
            {
                if(c[b] != -1 && c[b] != fragment[i][j]) ok = 0;
                c[b] = fragment[i][j];
                b++;
                b%=k;
            }
            if(!ok) continue;
            LL d = count(c,c+k,-1);
            LL dd = 1;
            RE(i,d) dd*=26-(k-d)-i;
            r+= dd%1000000009;
            
        }
        
        return r;
    }
};

class RandomOption
{
public:
    int badpair[15][15];
    LL dp[1<<14][15];
    int c;
    double f1(int i,int a,int b)
    {
        if(i == c)
        {
            return 1;
        }
        if(dp[a][b] != -1) return dp[a][b];
        double r = 0;
        RE(j,c)
        {
            if(!(a&1<<j) && (!badpair[j][b]) )r += f1(i+1,a|1<<j,j);
        }
        
        return (dp[a][b] = r);
    }
    double getProbability(int keyCount, vector <int> badLane1, vector <int> badLane2)
    {
        CLEAR(dp,-1);
        c = keyCount;
        CLEAR(badpair,0);
        RE(i,S(badLane1)) badpair[badLane1[i]][badLane2[i]] = 1,badpair[badLane2[i]][badLane1[i]] = 1;
        double r = 1;
        RE(i,keyCount)r*=i+1;
        double rr = 0; RE(i,c) rr+=f1(1, 1<<i, i);
        cout<<rr<<endl;
        return rr/r;
    }
    
    LL n,a,b;
    int v[15];
    int v2[15][15];

    double f2(int a, int b)
    {
        if (!a) {return 1; }
        if(dp[a][b] != -1) return dp[a][b];
        double r = 0;
        RE(i, n)
        {
            if (!v2[b][i] && a & 1<<i && b != i)
            {
                r += f2(a & ~(1 << i), i);
            }
        }
        return (dp[a][b] = r);
    }
    double getProbability2(int keyCount, vector <int> badLane1, vector <int> badLane2)
    {
        CLEAR(v2, 0);
        CLEAR(dp,-1);
        RE(i, S(badLane1)) v2[badLane1[i]][badLane2[i]] = 1, v2[badLane2[i]][badLane1[i]] = 1;
        n = keyCount;
        double r = 0;
        double a = 1; RE(i, keyCount) a *= i + 1;
        RE(i,keyCount) r += f2(((1<<n)-1) & ~(1 << i),i);
        return r / a;
    }
};
class TheCowDivTwo
{
public:
    int find5(int NN,int K)
    {
        LL dp[50][1001];
        CLEAR(dp,0);
        
        dp[0][0] = 1;
        REV(i,NN)
        REPV(j,1,K+1)
        RE(s,NN)
        dp[j][s] = dp[j][s] + dp[j-1][(s-i+NN)%NN],dp[j][s]%=1000000007;
        
        return dp[K][0];
    }
    int find4(int NN, int K)
    {
        LL dp[60][60][60];
        CLEAR(dp,0);
        RE(i,60) dp[i][0][0] = 1;
        REV(i,NN)
        REP(j,1,K+1)
        RE(s,NN)
        dp[i][j][s] = dp[i+1][j][s] + dp[i+1][j-1][(s-i+NN)%NN],dp[i][j][s]%=1000000007;
        
        return dp[0][K][0];
    }
    int find3(int NN, int K)
    {
        LL dp[60][60][60];
        CLEAR(dp,0);
        RE(i,NN+1) dp[i][0][0]=1;
        REP(i,1,NN+1)
        REP(j,1,K+1)
        RE(l,NN)
        {
            dp[i][j][l] = dp[i-1][j][l]+dp[i-1][j-1][(l-i+NN)%NN];
            dp[i][j][l] %=1000000007;
        }
        return (int)dp[NN][K][0];
    }
    int N;
    int f1(int n,int k,int s)
    {
        if(!k && !s) return 1;
        if(!k) return 0;
        if(n >= N) return 0;
        return f1(n+1,k,s)+f1(n+1,k-1,(s-n)%N);
    }
    int find(int NN, int K)
    {
        N = NN;
        return f1(0,K,0);
    }
    int find2(int NN, int K)
    {
        int N = NN;
        //int dp[10][503][503];
        int dp[48][1001][2];
        RE(i,2) dp[0][0][i] = 1;
        REV(j, NN)
        REP(i,1, K + 1)
        RE(l, NN)
        {
            dp[i][l][j%2] = (dp[i-1][(l - j + N) % N][(j + 1)%2] + dp[i][l][(j + 1)%2]) % 1000000007;
        }
        return dp[K][0][0];
        
    }
};

class SmallBricks31
{
public:
    int a[13];
    int w, hh;
    LL dp[11][1<<10];
    int countWays(int ww, int h)// this does not work because you need to combine 1-n, 2-n and 3-n blocks to build the tower
    {
        CLEAR(dp,0);
        RE(i,1<<ww)dp[0][i] = 1;
        REP(i,1,h+1)
        RE(j,1<<ww)
        RE(k,1<<ww)
        {
            if(~j & k) continue;
            dp[i][k]+=dp[i-1][j];
            dp[i][k]%=1000000007;
        }
        LL r=  0;
        RE(i,h)RE(j,1<<ww) r+=dp[i][j],r%=1000000007;
        return r+1;
    }
    LL f4(int h,int i,int current,int base)
    {
        if(i == w)
        {
            int nh = h-1;
            if(nh  == 0) return 1;
            if(dp[nh][current] != -1) return dp[nh][current];
            return (dp[nh][current] = f4(nh,0,0,current));
        }
        LL r = 0;
        r+=f4(h,i+1,current,base);
        if(base & 1<<i)
            r+=f4(h, i+1, current | 1<<i, base);
        r%=1000000007;
        if((base & 1<<i) && i+1 < w && (base & 1<<(i+1)))
            r+=f4(h, i+2, current | 1<<i | 1<<(i+1), base);
        r%=1000000007;
        if(i+2<w  && base & 1<<i && base & 1<<(i+2))
            r+=f4(h,i+3,current | 1<<i | 1<<(i+1) | 1<<(i+2),base);
        
        return r%1000000007;
    }
    int countWays234(int ww, int h)
    {
        w=ww;
        CLEAR(dp,-1);
        return f4(h,0,0,(1<<ww)-1);
    }
    int f3(int h,int i)
    {
        if(i == w) return 0;
        if(h == hh) return 0;
        LL r = 0;
        r+=f3(h,i+1);
        if(a[i] == h )
        {
            a[i]++;
            r += 1 + f3(h,i+1) + f3(h+1,0);
            a[i]--;
        }
        if(i+1<w && a[i] == h && a[i+1] == h)
        {
            a[i]++,a[i+1]++;
            r+= 1+f3(h,i+2)+f3(h+1,0);
            a[i]--,a[i+1]--;
        }
        if(i+2<w && a[i] == h && a[i+2] == h && a[i+1] <= a[i])
        {
            int t = a[i+1];
            a[i]++,a[i+2]++,a[i+1] = a[i];
            r += 1 + f3(h,i+3) + f3(h+1,0);
            a[i]--,a[i+2]--,a[i+1] = t;
        }
        return r%1000000007;
    }
    int countWays2(int ww, int h)
    {
        CLEAR(a,0);
        w = ww, hh = h;
        return f3(0,0);
    }
    int f2(int b)
    {
        int sc = 0;
        RE(i, w)
        {
            if (a[i] < hh && (b & 1<<i))
            {
                a[i]++;
                sc += 1+f2(b);
                a[i]--;
            }
            if (i + 1 < w && a[i] < hh && a[i + 1] == a[i] && (b & 1 << i) && (b & 1 << (i + 1)))
            {
                a[i]++, a[i + 1]++;
                sc += 1+f2(b);
                a[i]--, a[i + 1]--;
            }
            if (i + 2 < w && a[i] < hh && a[i + 2] == a[i] && a[i+1] <= a[i] && (b & 1 << i) && (b & 1 << (i + 2)))
            {
                int  t = a[i + 1];
                a[i]++, a[i + 2]++; a[i + 1] = a[i];
                sc += 1+f2(b);
                a[i]--, a[i + 2]--; a[i + 1] = t;
            }
        }
        return sc;
    }
    int countWays3(int ww, int h)
    {
        w = ww, hh = h;
        int r = 0;
        RE(i, 1 << ww)
        {
            r += f2(i);
        }
        return r+1;
    }
    
    int ww;
    void f1(int h,int k,int a,int b)
    {
        if(k == ww)
        {
            dp[h+1][a] += dp[h][b];
            dp[h+1][a] %= 1000000007;
            return;
        }
        f1(h,k+1,a,b);
        if(!(b&1<<k))
            f1(h,k+1,a | 1<<k,b);
        if(k+1 < ww && !(b&1<<k) && !(b&1<<(k+1)))
            f1(h,k+1,a | (1<<k) | (1 << (k+1)),b);
        if(k+2 < ww && !(b&1<<k) && !(b&1<<(k+2)))
            f1(h,k+1,a | (1<<k) | (1 << (k+2)),b);
    }
    int countWays345(int w, int h)
    {
        ww=w;
        CLEAR(dp,0);
        dp[0][0] = 1;
        RE(i,h)
        {
            RE(j,1<<w)
            {
                f1(i,0,0,j);
            }
        }
        LL r = 0;
        RE(i,h+1)RE(j,1<<w) r+=dp[h][j],r%=1000000007;
        return (int)r;

    }
};
class TheBrickTowerHardDivTwo
{
public:
    int find(int C, int K, int H)
    {
        int dp[4][4][4][4][8][48]; CLEAR(dp, 0);
        dp[0][0][0][0][0][0] = 1;
        REP(i, 1, H + 1)
        REP(a,1, C)REP(b,1, C)REP(c,1, C)REP(d,1, C)
        RE(aa, C)RE(bb, C)RE(cc, C)RE(dd, C)
        RE(j,K+1)
        {
            int f = 0;
            f += (aa == a) + (bb == b) + (cc == c) + (d == dd) + (a == b) + (a == c) + (d == b) + (d == c);
            if (f + j <= K)
            {
                dp[a][b][c][d][f+j][i] += dp[aa][bb][cc][dd][j][i - 1];
            }
        }
        int r = 0;
        REP(i, 1, H + 1)
        RE(a, C)RE(b, C)RE(c, C)RE(d, C)
        RE(j,K+1)
        r+=dp[a][b][c][d][j][i],r%=1000000007;
        
        return r;
        
    }
};
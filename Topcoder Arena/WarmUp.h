#include "Coder.h"


class WarmUp
{
public:
    bool runTests()
    {
        if(!testBubbleSort())
        {
            cout<<"Bubble Sort Failed"<<endl;
            return 0;
        }
        
        if(!testInsertionSort())
        {
            cout<<"Insertion Sort Failed"<<endl;
            return 0;
        }
        if(!testSelectionSort())
        {
            cout<<"Selection Sort Failed"<<endl;
            return 0;
        }
        if(!testQuickSort())
        {
            cout<<"Quick Sort Failed"<<endl;
            return 0;
        }
        if(!testHeapSort())
        {
            cout<<"Heap Sort Failed"<<endl;
            return 0;
        }
        
        if(!testKruskal())
        {
            cout<<"Graph: Kruskal minimum spanning tree failed"<<endl;
            return 0;
        }
        
        if(!testPrim())
        {
            cout<<"Graph: Prim's minimum spanning tree failed"<<endl;
            return 0;
        }

        
        cout<<"ALL TESTS SUCCEEDED"<<endl<<endl<<endl;
        return 1;
    }
private:
    
    VI bubbleSort(VI v)
    {
        RE(j,S(v))
        {
        {
            RE(i,S(v)-1)if(v[i]>v[i+1])swap(v[i],v[i+1]);
        }
        }
        
        return v;
    }
    bool testBubbleSort()
    {
        VI v = VI{3,3,5,6,8,2,1,-4,5,77};
        VI vSorted = v; SORT(vSorted);
        return vSorted  == bubbleSort(v);
    }
    VI selectionSort(VI v)
    {
        RE(i,S(v))REP(j,i+1,S(v))
        {
            if(v[j]<v[i])swap(v[i], v[j]);
        }
        return v;
    }
    bool testSelectionSort()
    {
        VI v = VI{3,3,5,6,8,2,1,-4,5,77};
        VI vSorted = v; SORT(vSorted);
        return vSorted  == selectionSort(v);
    }
    VI insertionSort(VI v)
    {
        VI a(S(v));
        VI b(S(v),1);
        RE(i,S(v)){
            int c = inf,cin = -1;
            RE(j,S(v)) {
                if(b[j] && v[j] < c)c=v[j],cin =j;
            }
            a[i]=c;
            b[cin]=0;
        }
        return a;
    }
    bool testInsertionSort()
    {
        VI v = VI{3,3,5,6,8,2,1,-4,5,77};
        VI vSorted = v; SORT(vSorted);
        return vSorted == insertionSort(v);
    }
    VI quickSort(VI v)
    {
        return qsort(v, 0, S(v));
    }
    VI qsort(VI v,int a,int b)
    {
        if(b-a <= 0) return v;
        int pivot = v[b-1];
        int x = a;
        REP(j,a,b-1)
        {
            if(v[j] <= pivot) swap(v[j], v[x]),x++;
        }
        swap(v[b-1], v[x]);
        v = qsort(v, a, x);
        v = qsort(v, x+1, b);
        return v;
    }
    bool testQuickSort()
    {
        VI v = VI{3,3,5,6,8,2,1,-4,5,77};
        VI vSorted = v; SORT(vSorted);
        return vSorted == quickSort(v);
    }
    VI heapSort(VI v)
    {
        VI a(S(v));
        REV(i,S(v)) v=heapify(v, i);
        RE(i,S(a))
        {
            a[i] = v[0];
            swap(v[0],v[S(v)-1]);
            v.erase(v.end()-1);
            v= heapify(v, 0);
        }
        return a;
    }
    VI heapify(VI v,int ind)
    {
        int i = ind;
        while(i < S(v))
        {
            ind  = i;
            int largest = v[ind];
            
            if(ind*2 < S(v) && v[ind*2] < largest)
                largest = v[ind*2],i = ind*2;
            if(ind*2+1 < S(v) && v[ind*2+1] < largest)
                largest = v[ind*2+1],i = ind*2+1;
            if(i == ind) break;
            swap(v[ind],v[i]);
                
            
        }
        return v;
    }
    bool testHeapSort()
    {
        VI v = VI{3,3,5,6,8,2,1,-4,5,77};
        VI vSorted = v; SORT(vSorted);
        return vSorted == heapSort(v);
    }
    VI unionSubsets(VI v,int x,int y)
    {
        int xp =x,yp =y;
        while(xp != v[xp]) xp=v[xp];
        while(yp != v[yp]) yp=v[yp];
        
        if(xp!=yp)
        {
            v[x] = yp; // pick any parent to be the parent of both of them
        }
        
        return v;
    }
    struct edge
    {
        int src,dest,weight;
        edge(int iSrc,int iDest,int iWeight):src(iSrc),dest(iDest),weight(iWeight){}
    };
    VVI minimumSpanningTreeKrusikal(vector<edge> edges, VI parents)
    {
        VVI edgesInOrder;
        RE(i,S(edges))RE(j,S(edges)-1) if(edges[j].weight > edges[j+1].weight) swap(edges[j], edges[j+1]);
        RE(i,S(parents)-1)
        {
            if(parents[edges[i].src ] == parents[edges[i].dest]) continue;
            edgesInOrder.push_back(VI{edges[i].src,edges[i].dest});
            unionSubsets(parents, edges[i].src,  edges[i].dest);
        }
        return edgesInOrder;
    }
    bool testKruskal()
    {
        int n = 4;
        VI parents; RE(i,n)parents.push_back(i);
        
        vector<edge> edges;
        edges.push_back(edge(0,1,10));
        edges.push_back(edge(0,2,6));
        edges.push_back(edge(0,3,5));
        edges.push_back(edge(1,3,15));
        edges.push_back(edge(2,3,4));
        
        VVI edgesInOrder  = minimumSpanningTreeKrusikal(edges, parents);
        VVI edgesInTest = VVI{VI{2,3},VI{0,3},VI{0,2}};
        
        return edgesInOrder == edgesInTest;
        
    }
    VI minimumSpanningTreePrim(vector<edge> edges,int n)
    {
        VI parents(n,-1);
        VI included(n,0);
        VI weights(n,inf);
        weights[0] = 0;
        
        RE(k,n-1)
        {
            int a = -1, aa = inf;
            RE(i,n)if(!included[i] && weights[i]<aa) aa= weights[i],a=i;
            included[a] = 1;
            
            RE(i,S(edges))
            {
                if(edges[i].src == a && !included[edges[i].dest] && edges[i].weight < weights[edges[i].dest])
                    parents[edges[i].dest] = edges[i].src, weights[edges[i].dest] = edges[i].weight;
            }
            
        }
        return parents;
    }
    
    bool testPrim()
    {
        int n = 5;
        
        vector<edge> edges;
        edges.push_back(edge(0,1,2));
        edges.push_back(edge(0,3,6));
        edges.push_back(edge(1,0,2));
        edges.push_back(edge(1,2,3));
        edges.push_back(edge(1,3,8));
        edges.push_back(edge(1,4,5));
        edges.push_back(edge(2,1,3));
        edges.push_back(edge(2,4,7));
        edges.push_back(edge(3,0,6));
        edges.push_back(edge(3,1,8));
        edges.push_back(edge(3,4,9));
        edges.push_back(edge(4,1,5));
        edges.push_back(edge(4,2,7));
        edges.push_back(edge(4,3,9));
        
        VI parents  = minimumSpanningTreePrim(edges, n);
        VI parentsInTest = VI{-1,0,1,0,1};
        
        return parents == parentsInTest;
        
    }
};





